<?php
if (!isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

require_once realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
require_once realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/books.php';
?>