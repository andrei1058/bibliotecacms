<?php 
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

$alreadyAdded = false;

if (isset($_POST['name']) && ! empty($_POST['name'])) {
    $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $stmt = $con->prepare("SELECT id FROM terms WHERE name = ?;");
    $stmt->bind_param('s', $_POST['name']);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows == 0) {
        $stmt = $con->prepare("INSERT INTO terms (name) VALUES (?);");
        $stmt->bind_param("s", $_POST['name']);
        $stmt->execute();
        mysqli_close($con);
        header("Location: $ROOT_DIR/");
        exit();
        return;
    } else {
        mysqli_close($con);
        $alreadyAdded = true;
    }
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
?>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Aggiungi Periodo</h5>
						<?php if ($alreadyAdded) { ?>
						    <div class="alert alert-dismissible alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p>Questo periodo e' gia' presente nel database.</p>
						</div>
						<?php }?>
						<form action="add_term" method="post" class="form-signin"
							enctype="multipart/form-data">
							<input type="text"
								name="name" class="form-control mb-2" placeholder="Nome"
								required> <input type="submit" value="Invia" name="add_term"
								class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>