<?php
ob_start();
if (isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

$badCredentials = false;

if (! empty($_POST)) {
    if ((isset($_POST['username']) && isset($_POST['password'])) && !(empty($_POST['username']) || empty($_POST['password']))) {
        // Getting submitted user data from database
        $con = new mysqli($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $stmt = $con->prepare("SELECT * FROM moderators WHERE username = ?");
        $stmt->bind_param('s', $_POST['username']);
        $stmt->execute();
        $result = $stmt->get_result();
        mysqli_close($con);
        if ($result->num_rows > 0) {

            $user = $result->fetch_object();

            // Verify user password and set $_SESSION
            if (password_verify($_POST['password'], $user->password)) {
                $_SESSION['username'] = $user->username;
                header("location: " . $ROOT_DIR . "/");
                exit();
                return;
            } else
                $badCredentials = true;
        } else $badCredentials = true;
    }
}
?>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Sign in</h5>
            	<?php if ($badCredentials) { ?>
            	  <div class="alert alert-dismissible alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Ouch!</strong> Credenziali errate!
						</div>
            	<?php }?>
            	<form action="" method="post" class="form-signin">
							<input type="text" name="username" class="form-control mb-2"
								placeholder="Nome utente" required autofocus> <input
								type="password" name="password" class="form-control"
								placeholder="Password" required> &nbsp; <input type="submit"
								value="Login"
								class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>