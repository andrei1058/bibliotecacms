<?php 
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/giudizio.php';

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ((isset($_GET["book"]) && isset($_GET["student"])) && !(empty($_GET["book"]) || empty($_GET["student"])) ){
    if (isset($_GET["comment"]) && !empty($_GET["comment"])){
        $comment = base64_encode($_GET["comment"]);
        setGiudizio($con, $_GET["student"], $_GET["book"], $_GET["comment"]);
        header("location: index"); exit();
        return;
    }
} else {
    header("location: index"); exit();
    return;
}

$comment = getGiudizio($con, $_GET["student"], $_GET["book"]);

function getBookName($con, $id)
{
    $rs = mysqli_query($con, "SELECT name FROM books WHERE id='$id';");
    if (! $rs)
        return "-";
        $row = mysqli_fetch_assoc($rs);
        return $row["name"];
}
?>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Aggiungi Commento</h5>
						<form action="comment" method="get" class="form-signin"
							enctype="multipart/form-data">
							<label for="name">Studente</label>
							<select class="form-control" name="student" required>
							<option value="<?php echo $_GET["student"]?>"><?php echo getStudentName($con, $_GET["student"])?></option>
							</select>
								
					        <label for="book">Libro</label>
							<select class="form-control" name="book" required>
							<option value="<?php echo $_GET["book"]?>"><?php echo getBookName($con, $_GET["book"])?></option>
							</select>
							<label for="comment">Commento</label>
							<textarea class="form-control" id="comment" name="comment" rows="3"><?php echo $comment?></textarea>

							<input type="submit" value="Salva"
								class="btn btn-lg btn-primary btn-block text-uppercase mt-2">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php mysqli_close($con);?>