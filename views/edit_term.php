<?php
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

$alreadyAdded = false;

if ((isset($_POST['name']) && isset($_POST['id'])) && !(empty($_POST['name']) || empty($_POST['id']))) {
    $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
    $stmt = $con->prepare("SELECT id FROM terms WHERE id = ?;");
    $stmt->bind_param('i', $_POST['id']);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows != 0) {
        $stmt = $con->prepare("UPDATE terms SET name=? WHERE id=?;");
        $stmt->bind_param("si", $_POST['name'], $_POST['id']);
        $stmt->execute();
        mysqli_close($con);
        header("Location: $ROOT_DIR/");
        exit();
        return;
    } else {
        mysqli_close($con);
        $alreadyAdded = true;
    }
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
?>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Modifica Periodo</h5>
						<?php if ($alreadyAdded) { ?>
						<div class="alert alert-dismissible alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p>Questo periodo non e' presente nel database.</p>
						</div>
						<?php }?>
						<form action="edit_term" method="post" class="form-signin"
							enctype="multipart/form-data">
						 <select
								class="form-control" name="id" required>
									<?php
        $conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $sql = "SELECT * FROM terms;";
        $rs = mysqli_query($conn, $sql);
        if (mysqli_num_rows($rs) > 0) {
            while ($row = mysqli_fetch_assoc($rs)) {
                echo "<option value='" . $row["id"] . "'>" . $row["name"] . "</option>";
            }
        }
        ?>
								</select> <input type="text" name="name"
								class="form-control mb-2 mt-2" placeholder="Nuovo nome" required> <input
								type="submit" value="Invia" name="edit_term"
								class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>