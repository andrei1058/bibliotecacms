<?php 
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

$badPassword = false;

if ((isset($_POST["password"]) && isset($_POST["password2"])) && ! (empty($_POST["password"]) || empty($_POST["password2"]))) {
    
    if ($_POST['password'] !== $_POST['password2']){
        $badPassword = true;
    } else {

        $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $stmt = $con->prepare("UPDATE moderators SET password=? WHERE username=?;");
        $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $stmt->bind_param('ss', $hash, $_SESSION['username']);
        $stmt->execute();

        header("location: logout");
        exit();
        return;
    }
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
?>

<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Nuova Password</h5>
						<?php if ($badPassword) { ?>
						    <div class="alert alert-dismissible alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p>Le password non coincidono.</p>
						</div>
						<?php }?>
						<form action="settings" method="post" class="form-signin"
							enctype="multipart/form-data">
							<input type="password" name="password" class="form-control mb-2"
								placeholder="Password" required autofocus>
								<input type="password" name="password2" class="form-control mb-2"
								placeholder="Ripeti password" required autofocus>
								<input type="submit" value="Salva" name="settings"
								class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>