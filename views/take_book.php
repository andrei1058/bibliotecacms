<?php
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';

if ((isset($_GET["book"]) && isset($_GET["student"]) && isset($_GET["term"])) && !(empty($_GET["book"]) || empty($_GET["student"]) || empty($_GET["term"]))) {
    $conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
    
    $stmt = $conn->prepare("INSERT INTO history (fk_student,fk_book,fk_term) VALUES (?,?,?);");
    $stmt->bind_param('iii', $_GET['student'],$_GET['book'],$_GET['term']);
    $stmt->execute();
    
    $stmt = $conn->prepare("UPDATE books SET status=?,reader=? WHERE id=?;");
    $status = 1;
    $stmt->bind_param('iii', $status, $_GET["student"], $_GET['book']);
    $stmt->execute();
    
    mysqli_close($conn);
    header("location: index"); exit(); return;
}

?>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Ritira Libro</h5>
						<form action="take_book" method="get" class="form-signin"
							enctype="multipart/form-data">
							<label for="term">Periodo</label>
							<select class="form-control mb-1" name="term" required>
<?php
$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
$sql = "SELECT id,name FROM terms;";
$rs = mysqli_query($conn, $sql);
if (mysqli_num_rows($rs) > 0) {
    while ($row = mysqli_fetch_assoc($rs)) {
        echo "<option value='" . $row["id"] . "'>" . $row["name"] . "</option>";
    }
}
?>
							</select>
							<label for="term">Libro</label>
							<select class="form-control mb-1" name="book" required>
<?php
$sql = "SELECT id,name,status FROM books;";
$rs = mysqli_query($conn, $sql);
if (mysqli_num_rows($rs) > 0) {
    while ($row = mysqli_fetch_assoc($rs)) {
        if ($row['status'] != 0) continue;
        echo "<option value='" . $row["id"] . "' ".((isset($_GET['book']) && $row["id"] == $_GET["boook"]) ? 'selected' : '').">" . $row["name"] . "</option>";
    }
}
?>
							</select>
							<label for="term">Studente</label>
							<select class="form-control mb-2" name="student" required>
<?php
$sql = "SELECT id,surname,name FROM students;";
$rs = mysqli_query($conn, $sql);
if (mysqli_num_rows($rs) > 0) {
    while ($row = mysqli_fetch_assoc($rs)) {
        echo "<option value='" . $row["id"] . "'>" . $row["surname"] . ' ' . $row["name"] . "</option>";
    }
}
mysqli_close($conn);
?>
							</select>
							<input type="submit" value="Invia" name="take_book" class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>