<?php
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';

if (isset($_GET["term"]) && !empty($_GET["term"])) {
    
} else {
    header("location: index"); exit();
    return;
}

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
$sql = "SELECT * FROM students ORDER BY surname ASC;";
$res = mysqli_query($con, $sql);
if (!$res) {
    echo "Nessun dato presente";
    return;
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/giudizio.php';

$pdf = new FPDF();
$pdf->AddPage();

$termName = getNomePeriodo($con, $_GET["term"]);
$pdf->SetFont('helvetica','',7);
$pdf->Cell(0,4,$termName,0,1,'C');

while ($row = mysqli_fetch_assoc($res)){
    $pdf->SetTitle($termName, true);
    $pdf->SetAutoPageBreak(true, 0);
    
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(5,8, $row["surname"] . " " . $row["name"]);
    
    $bks = mysqli_query($con, "SELECT fk_book FROM history WHERE fk_student=".$row["id"]." AND fk_term=".$_GET["term"].";");
    if ($bks) {
        while ($bks_res = mysqli_fetch_assoc($bks)) {
            $pdf->Ln();
            $pdf->SetFont('helvetica','I', '13');
            $pdf->Cell(5,6, getBookNameAndAuthor($con, $bks_res["fk_book"]));
            $pdf->Ln();
            $pdf->SetFont('Arial', '', '11');
            $giudizio = getGiudizio($con, $row["id"], $bks_res["fk_book"]);
            $size = strlen($giudizio);
            if (strlen($giudizio) > 108) {
                for ($i = 0; $i < $size/100; $i++) {
                    $part = substr($giudizio, $i*108, 108);
                    $pdf->Cell(5,5, $part);
                    $pdf->Ln();
                }
            } else {
                $pdf->Cell(5,5, getGiudizio($con, $row["id"], $bks_res["fk_book"]));
            }
        }
    }
    $pdf->Ln();
}
$pdf->Output($termName . ".pdf", "F");
mysqli_close($con);
?>
<embed src="<?php echo $termName?>.pdf" type="application/pdf" width="100%" height="600px" />