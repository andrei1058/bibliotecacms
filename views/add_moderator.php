<?php 
if (! isset($_SESSION['username'])) {
    require realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/views/login.php';
    return;
}

$badPassword = false;
$message = "Le password non coincidono.";

if ((isset($_POST["moderator"]) && isset($_POST["password"]) && isset($_POST["password2"])) && 
    !(empty($_POST["moderator"]) ||empty($_POST["password"]) || empty($_POST["password2"]))) {
    
    if ($_POST['password'] !== $_POST['password2']){
        $badPassword = true;
    } else {
        $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);

        $stmt = $con->prepare("SELECT id FROM moderators WHERE username=?;");
        $stmt->bind_param('s', $_POST['moderator']);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result == false || mysqli_num_rows($result) == 0) {            
            $stmt = $con->prepare("INSERT INTO moderators (username, password) VALUES (?,?);");
            $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $stmt->bind_param('ss', $_POST['moderator'], $hash);
            $stmt->execute();
            mysqli_close($con);
            header("location: index");
            exit();
            return;
        } else {
            echo $result;
            $badPassword = true;
            $message = "Questo moderatore e' gia' nel database.";
            mysqli_close($con);
        }
    }
}

include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . '/includes/header.php';
?>

<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Aggiungi Moderatore</h5>
						<?php if ($badPassword) { ?>
						    <div class="alert alert-dismissible alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p><?php echo $message;?></p>
						</div>
						<?php }?>
						<form action="add_moderator" method="post" class="form-signin"
							enctype="multipart/form-data">
							<input type="text" name="moderator" class="form-control mb-2"
								placeholder="Username" required autofocus>
							<input type="password" name="password" class="form-control mb-2"
								placeholder="Password" required autofocus>
								<input type="password" name="password2" class="form-control mb-2"
								placeholder="Ripeti password" required autofocus>
								<input type="submit" value="Salva" name="settings"
								class="btn btn-lg btn-primary btn-block text-uppercase">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>