<?php
include 'config.php';
$conn = new mysqli($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
$sql = "SELECT * FROM books;";
$rs = mysqli_query($conn, $sql);

if (mysqli_num_rows($rs) == 0) {
    ?>
<p class="text-center mt-5">Non e' stato aggiunto alcun libro.</p>
<?php

    return;
}

?>

<div class="container mt-3">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">Nome Libro</th>
				<th scope="col">Autore</th>
				<th scope="col">Stato</th>
				<th scope="col">Lettore Attuale</th>
			</tr>
		</thead>
		<tbody>
		<?php
        while ($row = mysqli_fetch_assoc($rs)) {
            ?>
			<tr>
				<td scope="row"><a href="book<?php echo $row["id"]?>"><?php echo $row["name"];?></a></td>
				<td><?php echo $row["author"];?></td>
				<td><?php echo getBookStatus($row["status"]);?></td>
				<td><a href="student<?php echo $row['reader']?>"><?php echo getReader($row["reader"], $conn)?></a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<?php mysqli_close($conn);

function getBookStatus($status){
    switch($status){
        default:
            return "<p style='color:red;';>Sconosciuto</p>";
        case "1":
            return "<p style='color:blue;';>Ritirato</p>";
        case "0":
            return "<p style='color:green;';>Disponibile</p>";
        case "2":
            return "<p style='color:red;';>Disattivato</p>";
    }
}

function getReader($userId, $con){
    if ($userId == -1) {
        return "-";
    }
    $rs = mysqli_query($con, "SELECT surname, name FROM students WHERE id='$userId';");
    if (!$rs) return "-";
    $row = mysqli_fetch_assoc($rs);
    return $row["surname"] . " " . $row["name"];
}