<?php

function displayStudent($con, $id)
{
    $rs = mysqli_query($con, "SELECT surname, name FROM students WHERE id='$id'");
    if (! $rs){
        return;
    }
    $row = mysqli_fetch_assoc($rs);
    
    include 'includes/giudizio.php';
    ?>
<div class="container mt-5">
	<div class="jumbotron">
		<h3 class="display-5 mb-5"><?php echo $row["surname"] . " " . $row["name"]?></h3>
		<?php
    $rs = mysqli_query($con, "SELECT * FROM history WHERE fk_student='$id';");
    if (!$rs) {
        echo "Nessun dato presente";
    } else {
        ?>
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">Nome Libro</th>
					<th scope="col">Autore</th>
					<th scope="col">Periodo</th>
					<th scope='col'>Giudizio</th>
					<th scope="col">Link</th>
				</tr>
			</thead>
			<tbody>
		<?php
        while ($row = mysqli_fetch_assoc($rs)) {
            ?>
				<tr>
<?php 
$res = mysqli_query($con, "SELECT id,name,author FROM books WHERE id='".$row['fk_book']."';");
if ($res) {
    $row2 = mysqli_fetch_assoc($res);
}
?>
					<td scope="row"><a href="book<?php echo $row2["id"]?>"><?php echo $row2["name"];?></a></td>
					<td scope="row"><?php echo $row2["author"];?></td>
					<td scope="row"><?php echo getPeriodName($con, $row["fk_term"]);?></td>
					<td scope="row"><?php echo getGiudizio($con, $row["fk_student"], $row["fk_book"]);?></td>
					<td scope='row'><a href="comment?student=<?php echo $row["fk_student"]?>&book=<?php echo $row["fk_book"]?>">Modifica giudizio</a></td>
				</tr>
				<?php } ?>
		</tbody>
		</table>
		<?php
    }
    ?>
	</div>
</div>
<?php
}

function getPeriodName($con, $id)
{
    $rs = mysqli_query($con, "SELECT name FROM terms WHERE id='$id';");
    if (! $rs)
        return "-";
    $row = mysqli_fetch_assoc($rs);
    return $row["name"];
}
?>