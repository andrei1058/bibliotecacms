<?php 
function getGiudizio($con, $userId, $bookId){
    $rs = mysqli_query($con, "SELECT comment FROM comment WHERE fk_student='$userId' AND fk_book='$bookId';");
    if (!$rs){
        return "Nessun giudizio aggiunto.";
    }
    $row = mysqli_fetch_assoc($rs);
    return base64_decode($row["comment"]);
}

function setGiudizio($con, $userId, $bookId, $message){
    $rs = mysqli_query($con, "SELECT id FROM comment WHERE fk_student='$userId' AND fk_book='$bookId';");
    if ($rs){
        $stmt = $con->prepare("UPDATE comment SET message=? WHERE fk_student=? AND fk_book=?;");
        $stmt->bind_param("sii", base64_encode($message), $userId, $bookId);
        $stmt->execute();
    } else {
        $stmt = $con->prepare("INSERT INTO comment (fk_student, fk_book, comment) VALUES (?, ?, ?);");
        $stmt->bind_param("iis", $userId, $bookId, base64_encode($message));
        $stmt->execute();
    }
}

function getStudentName($con, $id)
{
    $rs = mysqli_query($con, "SELECT surname,name FROM students WHERE id='$id';");
    if (! $rs)
        return "-";
        $row = mysqli_fetch_assoc($rs);
        return $row["surname"] . " " . $row["name"];
}

function getBookNameAndAuthor($con, $id)
{
    $rs = mysqli_query($con, "SELECT name,author FROM books WHERE id='$id';");
    if (! $rs)
        return "-";
        $row = mysqli_fetch_assoc($rs);
        return $row["name"] . " (" . $row["author"] . ")";
}

function getNomePeriodo($con, $id)
{
    $rs = mysqli_query($con, "SELECT name FROM terms WHERE id='$id';");
    if (! $rs)
        return "-";
        $row = mysqli_fetch_assoc($rs);
        return $row["name"];
}
?>