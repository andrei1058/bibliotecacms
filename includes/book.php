<?php
function displayBook($con, $id){    
    if (isset($_GET["action"]) && !empty($_GET["action"])) {
        $status = -1;
        switch ($_GET["action"]){
            case 'enter':
            case 'enable':
                $status = 0;
                break;
            case 'disable':
                $status = 2;
                break;
        }
        if ($status != -1) {
            $stmt = $con->prepare("UPDATE books SET status=?,reader=? WHERE id=?;");
            $reader = -1;
            $stmt->bind_param("iii", $status, $reader, $id);
            $stmt->execute();
        }
    }
    
    $rs = mysqli_query($con, "SELECT name,author,owner,status FROM books WHERE id='$id'");
    if (! $rs) {
        return;
    }
    $row = mysqli_fetch_assoc($rs);
    include 'includes/giudizio.php';
    
    echo "<div class='container mt-5'>";
    echo "<div class='jumbotron'>";
    echo "<h3 class='display-4 mb-1'>".$row["name"]."</h3>";
    echo "<h5 class='display-6 mb-1'>Autore: ".$row["author"]."</h5>";
    echo "<h5 class='display-6 mb-1'>Proprietario: ". getStudentName($con, $row["owner"])."</h5>";
    echo "<div class='row d-block ml-0 mt-3'>";
    
    if ($row['status'] == 0) {
        echo "<a href='take_book?book=$id'><button type='button m-t5' class='btn btn-primary mr-1'>Ritiro Libro</button></a>";
    } else if ($row['status'] == 1){
        echo "<a href='book$id?action=enter'><button onclick='window.location.href = 'book&action=enter';' type='button m-t5' class='btn btn-warning mr-1'>Rientro Libro</button></a>";
    }
    
    if ($row['status'] != 2) {
        echo "<a href='book$id?action=disable'><button type='button m-t5' class='btn btn-warning mr-1'>Disattiva Libro</button></a>";
    } else {
        echo "<a href='book$id?action=enable'><button type='button m-t5' class='btn btn-primary mr-1'>Attiva Libro</button></a>";
    }
    
    echo "</div>";

    $rs = mysqli_query($con, "SELECT * FROM history WHERE fk_book='$id';");
    if (! $rs) {
        echo "<p class='d-block mt-5'>Nessun dato presente</p>";
    } else {
        echo "<table class='table table-hover d-block mt-5'>";
        echo "<thead><tr>";
        echo "<th scope='col'>Nome Lettore</th>";
        echo "<th scope='col'>Periodo</th>";
        echo "<th scope='col'>Giudizio</th>";
        echo "<th scope='col'>Link</th>";
        echo "</tr></thead>";
        
        echo "<tbody><tr>";
        while ($row = mysqli_fetch_assoc($rs)) {
            echo "<tr>";
            echo "<td scope='row'><a href='student".$row["fk_student"]."'>". getStudentName($con, $row["fk_student"]) . "</a></td>";
            echo "<td scope='row'>" . getNomePeriodo($con, $row["fk_term"]) . "</td>";
            echo "<td scope='row'>". getGiudizio($con, $row["fk_student"], $row["fk_book"]) ."</td>";
            echo "<td scope='row'><a href='comment?student=".$row["fk_student"]."&book=".$row["fk_book"]."'>Modifica giudizio</a></td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    }
    echo "</div>";
    echo "</div>";
}
?>