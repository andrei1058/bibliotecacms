<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<a class="navbar-brand" href="<?php echo $ROOT_DIR?>"><?php echo $HEADER_NAME;?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarColor01" aria-controls="navbarColor01"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarColor01">
		<ul class="navbar-nav mr-auto">

			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
				data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
				aria-expanded="false">Account</a>
				<div class="dropdown-menu" x-placement="bottom-start"
					style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
					<a class="dropdown-item" href="add_student">Aggiungi Studente</a> <a
						class="dropdown-item" href="add_moderator">Aggiungi Moderatore</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="settings">Modifica Account</a> <a
						class="dropdown-item" href="logout">Esci</a>
				</div></li>

			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
				data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
				aria-expanded="false">Periodi</a>
				<div class="dropdown-menu" x-placement="bottom-start"
					style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
					<a class="dropdown-item" href="add_term">Aggiungi Periodo</a> <a
						class="dropdown-item" href="edit_term">Modifica Periodo</a>
						<?php 
						$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
						$res = mysqli_query($conn, "SELECT id,name FROM terms;");
						mysqli_close($conn);
						if ($res) {
						    echo '<div class="dropdown-divider"></div>';
						    while ($row = mysqli_fetch_assoc($res)) {
						        echo '<a class="dropdown-item" href="download_term?term='.$row["id"].'">Scarica Scheda '.$row["name"].'</a>';
						    }
						}
						?>
				</div></li>
				
				
		    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
				data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
				aria-expanded="false">Libro</a>
				<div class="dropdown-menu" x-placement="bottom-start"
					style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
					<a class="dropdown-item" href="add_book">Aggiungi Libro</a>
						<a class="dropdown-item" href="take_book">Ritira Libro</a>
				</div></li>
		</ul>
		
		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="text"
				placeholder="Cerca libro">
			<button class="btn btn-secondary my-2 my-sm-0" type="submit">Cerca</button>
		</form>
	</div>
</nav>