<?php 
if(session_status()!=PHP_SESSION_ACTIVE){ 
    session_start();
}
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $WEBSITE_NAME; ?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.css">
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<link rel="shortcut icon" type="image/png" href="favicon.png" />
</head>
<body>
<?php
session_start();
$request = str_replace($ROOT_DIR, "", $_SERVER['REQUEST_URI']);
$get = preg_split("/\?/",$request, 2);
$request = $get[0];

$routed = false;

if (isset($_SESSION['username'])) {
    if (substr($request, 1, 7) === "student") {
        $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $rs = mysqli_query($con, "SELECT id FROM students;");
        if (!$rs) {
            http_response_code(404);
            require 'views/404.php';
            $routed = true;
        } else {
            while ($row = mysqli_fetch_assoc($rs)) {
                $id = $row["id"];
                if ($request == "/student" . $id) {
                    include 'includes/header.php';
                    require 'includes/student.php';
                    displayStudent($con, $id);
                    $routed = true;
                    mysqli_close($con);
                    break;
                }
            }
        }
    } else if (substr($request, 1, 4) === "book") {
        $con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
        $rs = mysqli_query($con, "SELECT id FROM books;");
        if (!$rs) {
            http_response_code(404);
            require 'views/404.php';
            $routed = true;
        } else {
            while ($row = mysqli_fetch_assoc($rs)) {
                $id = $row["id"];
                if ($request == "/book" . $id) {
                    include 'includes/header.php';
                    require 'includes/book.php';
                    displayBook($con, $id);
                    $routed = true;
                    mysqli_close($con);
                    break;
                }
            }
        }
    }
}

if (! $routed) {
    switch ($request) {
        case '/':
        case '':
        case '/home':
        case '/index':
            require 'views/index.php';
            break;
        case '/login':
            require 'views/login.php';
            break;
        case '/logout':
            require 'views/logout.php';
            break;
        case '/add_student':
            require 'views/add_student.php';
            break;
        case '/add_book':
            require 'views/add_book.php';
            break;
        case '/add_term':
            require 'views/add_term.php';
            break;
        case '/edit_term':
            require 'views/edit_term.php';
            break;
        case '/take_book':
            require 'views/take_book.php';
            break;
        case '/comment':
            require 'views/comment.php';
            break;
        case '/download_term':
            require('fpdf.php');
            require 'views/download_term.php';
            break;
        case '/settings':
            require 'views/settings.php';
            break;
        case '/add_moderator':
            require 'views/add_moderator.php';
            break;
        default:
            http_response_code(404);
            require 'views/404.php';
            break;
    }
}

?>
</body>
</html>